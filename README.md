# car_usb_power_adapter

Kicad PCB project for a simple USB car power adapter

***

![Car USB Power Adapter](./car_usb_power_adapter.png)*Car USB Power Adapter*


## Description
Simple USB car power adapter for the auxiliaries 12V power outlet


## License
OSH license applies.
